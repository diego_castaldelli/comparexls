package comparexls.comparexls;

import java.io.File;
import java.io.IOException;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class App {
	
    public static void main( String[] args ) throws BiffException, IOException {
    	
    	File file = new File("C:\\Users\\Diego\\Desktop\\livro.xls");
    	Workbook workbook = Workbook.getWorkbook(file);
    	Sheet sheet = workbook.getSheet(0); 
    	int rows = sheet.getRows();
    	
    	for (int i = 0; i < rows; i++) {
    		Cell a1 = sheet.getCell(0, i);
    		System.out.println(a1.getContents());
		}
    	
    	workbook.close();
    }
     
}
